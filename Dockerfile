FROM node:14

WORKDIR /app

COPY ./package*.json ./

RUN npm install --save express ejs

COPY . .

EXPOSE 3005

CMD ["npm", "start", "echo", "Hello from Docker"]
